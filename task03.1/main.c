/****************************************************************************
 *                                                                          *
 * File    : main.c                                                         *
 *                                                                          *
 * Purpose : Console mode (command line) program.                           *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/

#include <stdio.h>

/****************************************************************************
 *                                                                          *
 * Function: main                                                           *
 *                                                                          *
 * Purpose : Main entry point.                                              *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/
/* Даны три числа. Найти сумму двух наибольших из них. */

int main(int argc, char *argv[])
{
	float a, b, c, s;

	scanf("%f %f %f", &a, &b ,&c);

	if (a>=c&&b>c){
		s=a+b;
	}else if (b>=a&&c>a){
		s=b+c;
	}else if (c>=b&&a>b){
		s=c+a;
	}else{
		s=a+b;
	}

	printf("%f\n",s );

	return 0;
}

