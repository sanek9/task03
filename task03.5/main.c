/****************************************************************************
 *                                                                          *
 * File    : main.c                                                         *
 *                                                                          *
 * Purpose : Console mode (command line) program.                           *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/

#include <stdio.h>

/****************************************************************************
 *                                                                          *
 * Function: main                                                           *
 *                                                                          *
 * Purpose : Main entry point.                                              *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/
/* Даны координаты двух различных полей шахматной доски x1, y1, x2, y2 (целые 
 числа, лежащие в диапазоне 1–8). Проверить истинность высказывания:
 «Данные поля имеют одинаковый цвет». */

int main(int argc, char *argv[])
{
	int x1, x2, y1, y2;
	scanf("%d %d", &x1, &y1);
	scanf("%d %d", &x2, &y2);
	//printf("%d\n",((x1+y1)%2) == ((x2+y2)%2) );
	if(((x1+y1)%2) == ((x2+y2)%2)){
		printf("true\n");
	}else{
		printf("false\n");
	}
	return 0;
}

