all: task031 task032 task033 task034 task035

task031: ./task03.1/main.c
	gcc ./task03.1/main.c -o task031
task032: ./task03.2/main.c
	gcc ./task03.2/main.c -o task032
task033: ./task03.3/main.c
	gcc ./task03.3/main.c -o task033
task034: ./task03.4/main.c
	gcc ./task03.4/main.c -o task034
task035: ./task03.5/main.c
	gcc ./task03.5/main.c -o task035
clean:
	rm task031 task032 task033 task034 task035
